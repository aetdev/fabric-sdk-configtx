
let path = require('path');
const fs = require('fs-extra');

const Application = require('./classes/application');
const Consortium = require('./classes/consortium');
const Orderer = require('./classes/orderer');
const Organization = require('./classes/organization');
const Profile = require('./classes/profile');
const Resources = require('./classes/resources');


const ConfigTX = class {
    constructor(organizations, orderer, profiles, capabilities, application, resources, channel) {

        if (organizations) {
            if (!Array.isArray(organizations)) {
                throw new Error("the 'organizations' parameter is not an array");
            }
            this.Organizations = organizations;
        }

        if (capabilities) {
            if (!(typeof capabilities === "object")) {
                throw new Error("the 'capabilities' parameter is not object");
            }
            this.Capabilities = capabilities;
        }

        if (application) {
            if (!(application instanceof Application)) {
                throw new Error("the 'application' parameter is not an instance class Application");
            }
            this.Application = application;
        }

        if (resources) {
            if (!(resources instanceof Resources)) {
                throw new Error("the 'resources' parameter is not an instance class Resources");
            }
            this.Resources = resources;
        }

        if (orderer) {
            if (!(orderer instanceof Orderer)) {
                throw new Error("the 'orderer' parameter is not an instance class Orderer");
            }
            this.Orderer = orderer;
        }

        if (profiles) {
            if (!(typeof profiles === "object")) {
                throw new Error("the 'profiles' parameter is not object");
            }
            this.Profiles = profiles;
        }

        if (channel) {
            if (!(typeof profiles === "object")) {
                throw new Error("the 'channel' parameter is not object");
            }
            this.Channel = channel;
        }
    }

    setOrderer(addresses_or_orderer, type = "solo", batchTimeout = "2s", batchSize, kafka, organizations, capabilities, maxChannels) {
        if (!this.hasOwnProperty("Orderer")) {
            this.Orderer = {};
        }

        if (addresses_or_orderer instanceof Orderer) {
            this.Orderer = addresses_or_orderer;
        } else {
            this.Orderer = new Orderer(addresses_or_orderer, type = "solo", batchTimeout = "2s", batchSize, kafka, organizations, capabilities, maxChannels)
        }
    }

    addOrg (name_or_org, mspdir, anchorPeers, id, mspType, adminPrincipal) {
        if (!this.hasOwnProperty("Organizations")) {
            this.Organizations = [];
        }

        if (name_or_org instanceof Organization) {
            this.Organizations.push(name_or_org);
        } else {
            let org = new Organization(name_or_org, mspdir, anchorPeers, id, mspType, adminPrincipal);
            this.Organizations.push(org);
        }
    }

    addProfile(name, consortium_or_profile, application, orderer, consortiums, capabilities) {
        if (!this.hasOwnProperty("Profiles")) {
            this.Profiles = {};
        }

        if (consortium_or_profile instanceof Profile) {
            this.Profiles[name] = consortium_or_profile;
        } else {
            this.Profiles[name] = new Profile(consortium_or_profile, application, orderer, consortiums, capabilities);
        }
    }

    setApplication (organizations_or_app, capabilities, resources, policies, acls) {
        if (!this.hasOwnProperty("Application")) {
            this.Application = {};
        }

        if (organizations_or_app instanceof Application) {
            this.Application = organizations_or_app;
        } else {
            this.Application = new Application(organizations_or_app, capabilities, resources, policies, acls);
        }
    }

    setResources (resources) {
        if (resources instanceof Resources) {
            this.Resources = resources;
        } else {
            this.Resources = new Resources(resources);
        }
    }

    setChannel(consortium_or_channel, application, orderer, consortiums, capabilities) {

        if (consortium_or_channel instanceof Profile) {
            this.Channel = consortium_or_channel;
        } else {
            this.Channel = new Profile(consortium_or_channel, application, orderer, consortiums, capabilities);
        }
    }

    addCapability(type, capability) {
        if (!type) {
            throw new Error('Missing type parameter');
        }

        if (!(typeof capability === "object")) {
            throw new Error('Missing capability parameter');
        }

        if (!this.hasOwnProperty("Capabilities")) {
            this.Capabilities = {};
        }

        return this.Capabilities[type] = capability;
    }

    toJson(format = false) {

        let json = "";

        if (format) {
            json = JSON.stringify(this, null, 2);
        } else {
            json = JSON.stringify(this);
        }

        return json;
    }

    saveToFile(configPath, filename = "configtx.json") {
        configPath = (configPath) ? configPath : this.constructor.configFileLocation;

        if (!fs.pathExistsSync(configPath)){
            fs.ensureDirSync(configPath);
        }

        fs.writeFileSync(configPath+filename, this.toJson(true));
    }


    static get configFileLocation() {
        return path.join(RootPATH , '../network/config/network/');
    }
};













module.exports = ConfigTX;
module.exports.Resources = Resources;
module.exports.Application = Application;
module.exports.Organization = Organization;
module.exports.Orderer = Orderer;
module.exports.Consortium = Consortium;
module.exports.Profile = Profile;