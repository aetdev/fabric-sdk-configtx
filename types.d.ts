
declare class ConfigTX {
    Organizations?: ConfigTX.Organization[];
    Capabilities?:  { [s: string]: ConfigTX.CapabilitiesMap; };
    Application?: ConfigTX.Application;
    Orderer?: ConfigTX.Orderer;
    Profiles?: ConfigTX.ProfilesMap;
    Resources?: ConfigTX.Resources;
    Channel?: ConfigTX.Profile;

    constructor(
        organizations?: ConfigTX.Organization[],
        orderer?: ConfigTX.Orderer,
        profiles?: ConfigTX.ProfilesMap,
        capabilities?:  { [s: string]: ConfigTX.CapabilitiesMap; },
        application?: ConfigTX.Application,
        resources?: ConfigTX.Resources,
        channel?: ConfigTX.Profile
    );

    /**
     * This section defines the values to encode into a config transaction or  genesis block for orderer related parameters
     * @param addresses
     * @param type: The orderer implementation to start. Available types are "solo" and "kafka"
     * @param batchTimeout: The amount of time to wait before creating a batch
     * @param batchSize: Controls the number of messages batched into a block
     * @param kafka
     * @param organizations
     * @param capabilities
     * @param maxChannels
     */
    public setOrderer(
        addresses: ConfigTX.Orderer | string[],
        type?: string,
        batchTimeout?: string,
        batchSize?: ConfigTX.BatchSize,
        kafka?: ConfigTX.Kafka,
        organizations?: ConfigTX.Organization,
        capabilities?: ConfigTX.CapabilitiesMap,
        maxChannels?: number
    ): void;

    /**
     * This section defines the different organizational identities which will   be referenced later in the configuration.
     * @param name
     * @param mspdir MSPDir is the filesystem path which contains the MSP configuration
     * @param anchorPeers  defines the location of peers which can be used for cross org gossip communication.  Note, this value is only encoded in the genesis block in the Application section context
     * @param id ID to load the MSP definition as
     * @param mspType
     * @param adminPrincipal
     */
    public addOrg(
        name: ConfigTX.Organization | string,
        mspdir?: string,
        anchorPeers?: ConfigTX.AnchorPeer[] | string,
        id?: string,
        mspType?: string,
        adminPrincipal?: string
    ): void;

    /**
     * Different configuration profiles may be encoded here to be specified as parameters to the configtxgen tool
     * @param name
     * @param consortium
     * @param application
     * @param orderer
     * @param consortiums
     * @param capabilities
     */
    public addProfile(
        name: string,
        consortium?: ConfigTX.Profile | string,
        application?: ConfigTX.Application,
        orderer?: ConfigTX.Orderer,
        consortiums?: ConfigTX.ConsortiumsMap,
        capabilities?: ConfigTX.CapabilitiesMap
    ): void;


    public setApplication (
        organizations_or_app?: ConfigTX.Application | ConfigTX.Organization[],
        capabilities?: ConfigTX.CapabilitiesMap,
        resources?: ConfigTX.Resources
    ): void;

    public setResources (resources?: ConfigTX.Resources): void;

    public setChannel(
        consortium?: ConfigTX.Profile | string,
        application?: ConfigTX.Application,
        orderer?: ConfigTX.Orderer,
        consortiums?: ConfigTX.ConsortiumsMap,
        capabilities?: ConfigTX.CapabilitiesMap
    ): void;

    public addCapability(type: 'Channel' | 'Orderer' | 'Application', capability: ConfigTX.CapabilitiesMap): ConfigTX.CapabilitiesMap;


    public toJson(format?: boolean): string;

    public saveToFile(configPath?: string, filename?: string): void;

    public static configFileLocation(): string;

}
export = ConfigTX;

declare namespace ConfigTX {

    /**
     * Organization encodes the organization-level configuration needed in config transactions.
     */
    export class Organization {
        Name: string;
        ID: string;
        MSPDir: string;
        MSPType?: string;
        AnchorPeers?: AnchorPeer[];
        Policies?: PoliciesMap;

        constructor(
            name: string,
            mspdir: string,
            anchorPeers?: ConfigTX.AnchorPeer[] | ConfigTX.AnchorPeer,
            id?: string,
            mspType?: string,
            policies?: ConfigTX.PoliciesMap,
        );

        public addAnchorPeer(anchorPeer: ConfigTX.AnchorPeer): void;

        public addPolicy(type: PolicyType, policy: Policy): void;
    }

    /**
     * Orderer contains configuration which is used for the bootstrapping of an orderer by the provisional bootstrapper.
     */
    export class Orderer {
        OrdererType: string;
        Addresses: string[]|string;
        BatchTimeout?: string;
        BatchSize?: BatchSize;
        Kafka?: Kafka;
        EtcdRaft?: etcdraft.Metadata;
        Organizations?: Organization[];
        MaxChannels?: number;
        Capabilities?: CapabilitiesMap;
        Policies?: PoliciesMap;

        constructor(
            addresses: string[],
            type?: string,
            batchTimeout?: string,
            batchSize?: ConfigTX.BatchSize,
            kafka?: ConfigTX.Kafka | KafkaBroker | KafkaBrokers,
            organizations?: ConfigTX.Organization[]|ConfigTX.Organization,
            capabilities?: ConfigTX.CapabilitiesMap,
            maxChannels?: number,
            policies?: ConfigTX.PoliciesMap,
        );

        public addOrg(org: Organization): void;

        public addOrgs(orgsArr: Organization[]): void;

        public setKafka(kafka: Kafka): void;

        public addAddress(address: string): void;

        public addPolicy(type: PolicyType, policy: Policy): void;
    }

    /**
     * Profile encodes orderer/application configuration combinations for the configtxgen tool.
     */
    export class Profile {
        Consortium?: string;
        Application?: Application;
        Orderer?:  Orderer;
        Consortiums?: ConsortiumsMap;
        Capabilities?: CapabilitiesMap;
        Policies?: PoliciesMap;

        constructor(
            consortium?: string,
            application?: ConfigTX.Application,
            orderer?: ConfigTX.Orderer,
            consortiums?: ConfigTX.ConsortiumsMap,
            capabilities?: ConfigTX.CapabilitiesMap,
            policies?: ConfigTX.PoliciesMap
        )

        public addConsortium(
            name: string,
            orgs_or_consortium: Consortium | Organization[]
        ): void;

        public setApplication(
            organizations_or_app: ConfigTX.Application | ConfigTX.Organization[],
            capabilities?: ConfigTX.CapabilitiesMap,
            resources?: ConfigTX.Resources): void;

        public setOrderer(orderer: ConfigTX.Orderer): void;


        public addPolicy(type: PolicyType, policy: Policy): void;

        public addCapability(name: string|CapabilitiesMap, val?: boolean): void;
    }

    /**
     * Consortium represents a group of organizations which may create channels with each other
     */
    export class Consortium {
        Organizations: Organization[];

        constructor(organizations: Organization[]);

        public addOrg(org: Organization): void;
    }

    /**
     * Application encodes the application-level configuration needed in config transactions.
     */
    export class Application {
        Organizations?: Organization[];
        Capabilities?: CapabilitiesMap;
        Resources?: Resources;
        Policies?: PoliciesMap;
        ACLs?: ACLsMap;

        constructor(
            organizations?: Organization[],
            capabilities?: CapabilitiesMap,
            resources?: Resources,
            policies?: PoliciesMap,
            acls?: ACLsMap
        )

        public addOrg(org: Organization): void;

        public setResources(resources: ConfigTX.Resources): void;

        public addPolicy(type: PolicyType, policy: Policy): void;
    }

    /**
     * Resources encodes the application-level resources configuration needed to seed the resource tree
     */
    export class Resources {
        DefaultModPolicy: string;

        constructor(defaultModPolicy: string)
    }

    /**
     * Kafka contains configuration for the Kafka-based orderer.
     * @param Brokers: A list of Kafka brokers to which the orderer connects
     * NOTE: Use IP:port notation
     */
    export class Kafka {
        Brokers: KafkaBrokers;

        constructor (brokers?: KafkaBroker|KafkaBrokers);

        public addBroker(broker: KafkaBroker): void;
    }

    export type KafkaBroker = string;
    export type KafkaBrokers = KafkaBroker[];

    /**
     * BatchSize contains configuration affecting the size of batches.
     * @param MaxMessageCount: The maximum number of messages to permit in a batch
     * @param AbsoluteMaxBytes: The absolute maximum number of bytes allowed for the serialized messages in a batch.
     * @param PreferredMaxBytes: The preferred maximum number of bytes allowed for the serialized messages in a batch. A message larger than the preferred max bytes will result in a batch larger than preferred max bytes.
     */
    export type BatchSize = {
        MaxMessageCount?: number;
        AbsoluteMaxBytes?: string;
        PreferredMaxBytes?: string;
    }

    /**
     * AnchorPeer encodes the necessary fields to identify an anchor peer.
     */
    export type AnchorPeer = {
        Host: string;
        Port: number;
    }

    /**
     * Policy encodes a channel config policy
     */
    export type Policy = {
        Type: string;
        Rule: string;
    }

    export type PolicyType = 'Readers' | 'Writers' | 'Admins' | 'LifecycleEndorsement' | 'Endorsement' | 'BlockValidation';

    export interface ProfilesMap {
        [s: string]: Profile;
    }

    export interface ConsortiumsMap {
        [s: string]: Consortium;
    }

    export interface CapabilitiesMap {
        [s: string]: boolean;
    }

    export interface ACLsMap {
        [s: string]: string;
    }

    export interface PoliciesMap {
        [s: string]: Policy;
    }



}

declare namespace etcdraft {
    /**
     * Metadata is serialized and set as the value of ConsensusType.Metadata in
     * a channel configuration when the ConsensusType.Type is set "etcdraft".
     */
    class Metadata {
        Consenters: Consenter[];
        Options: Options;
        XXX_NoUnkeyedLiteral: object;
        XXX_unrecognized: string;
        XXX_sizecache: number;
    }

    /**
     * Consenter represents a consenting node (i.e. replica).
     */
    class Consenter {
        Host: string;
        Port: number;
        ClientTlsCert: string;
        ServerTlsCert: string;
        XXX_NoUnkeyedLiteral: {};
        XXX_unrecognized: string;
        XXX_sizecache: number;
    }

    /**
     * Options to be specified for all the etcd/raft nodes. These can be modified on a per-channel basis.
     */
    class Options {
        TickInterval: number;
        ElectionTick: number;
        HeartbeatTick: number;
        MaxInflightMsgs: number;
        MaxSizePerMsg: number;
        SnapshotInterval: number;
        XXX_NoUnkeyedLiteral: {};
        XXX_unrecognized: string;
        XXX_sizecache: number;
    }
}