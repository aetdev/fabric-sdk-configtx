const Organization = class{
    constructor(name, mspdir, anchorPeers, id, mspType, policies) {
        if (!name) {
            throw new Error('Missing name parameter');
        }
        if (!mspdir) {
            throw new Error('Missing mspdir parameter');
        }

        this.Name = name;
        this.ID = (id) ? id : name+"MSP";
        this.MSPDir = mspdir;


        if (mspType) {
            this.MSPType = mspType;
        }

        if (anchorPeers) {
            this.AnchorPeers = [];

            if (typeof anchorPeers === 'object') {
                this.AnchorPeers.push(anchorPeers);
            } else if (Array.isArray(anchorPeers)) {
                this.AnchorPeers = anchorPeers;
            }
        }

        if (policies) {
            this.Policies = policies;
        }
    }

    addPolicy(type, policy) {
        if (!type) {
            throw new Error('Missing type parameter');
        }

        if (!(typeof policy === "object")) {
            throw new Error('Missing policy parameter');
        }

        if (!this.hasOwnProperty("Policies")) {
            this.Policies = {};
        }

        this.Policies[type] = policy;
    }

    addAnchorPeer(anchorPeer) {
        if (!this.hasOwnProperty("AnchorPeers")) {
            this.AnchorPeers = [];
        }

        this.AnchorPeers.push(anchorPeer);
    }
};

module.exports = Organization;