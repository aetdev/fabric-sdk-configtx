const Application = require('./application');
const Consortium = require('./consortium');
const Orderer = require('./orderer');
const Organization = require('./organization');

const Profile = class{
    constructor(consortium, application, orderer, consortiums, capabilities, policies) {

        if (consortium) {
            this.Consortium = consortium;
        }

        if (application) {
            if (!application instanceof Application) {
                throw new Error('application parameter not instance of Application class');
            }

            this.Application = application;
        }

        if (orderer) {
            if (!orderer instanceof Orderer) {
                throw new Error('orderer parameter not instance of Orderer class');
            }

            this.Orderer = orderer;
        }

        if (consortiums) {
            this.Consortiums = consortiums;
        }

        if (capabilities) {
            this.Capabilities = capabilities;
        }

        if (policies) {
            this.Policies = policies;
        }
    }

    addConsortium(name, orgs_or_consortium) {
        if (!name) {
            throw new Error('Missing name parameter');
        }

        this.Consortiums = {};

        if (orgs_or_consortium instanceof Consortium) {
            this.Consortiums[name] = orgs_or_consortium;
        } else if (Array.isArray(orgs_or_consortium) || orgs_or_consortium instanceof Organization) {
            this.Consortiums[name] = new Consortium(orgs_or_consortium)
        }

    }

    setApplication(organizations_or_app, capabilities, resources) {

        this.Application = {};

        if (organizations_or_app instanceof Application) {
            this.Application = organizations_or_app;
        } else {
            this.Application = new Application(organizations_or_app, capabilities, resources)
        }
    }

    setOrderer(orderer) {
        if (!orderer instanceof Orderer) {
            throw new Error('orderer parameter not instance of Orderer class');
        }

        this.Orderer = orderer;
    }

    addPolicy(type, policy) {
        if (!type) {
            throw new Error('Missing type parameter');
        }

        if (!(typeof policy === "object")) {
            throw new Error('Missing policy parameter');
        }

        if (!this.hasOwnProperty("Policies")) {
            this.Policies = {};
        }

        this.Policies[type] = policy;
    }

    addCapability(name_or_capability, val) {
        if (!name_or_capability) {
            throw new Error('Missing name_or_capability parameter');
        }

        if (!this.hasOwnProperty("Capabilities")) {
            this.Capabilities = {};
        }

        if (typeof name_or_capability === "object") {
            this.Capabilities = Object.assign(this.Capabilities, name_or_capability);
        } else {
            if (!(typeof val === "boolean")) {
                throw new Error('Missing val parameter');
            }

            this.Capabilities[name] = val;
        }
    }
};

module.exports = Profile;