const Resources = class{
    constructor(defaultModPolicy) {
        if (!defaultModPolicy) {
            throw new Error('Missing defaultModPolicy parameter');
        }

        this.DefaultModPolicy = defaultModPolicy;
    }

};

module.exports = Resources;