const Resources = require('./resources');
const Organization = require('./organization');

const Application = class{
    constructor(organizations, capabilities, resources, policies, acls) {

        this.Organizations = null;

        if (organizations){
            this.Organizations = organizations;
        }

        if (capabilities){
            this.Capabilities = capabilities;
        }

        if (resources){
            this.Resources = {};

            if (typeof resources === 'string') {
                this.Resources = new Resources(resources);
            } else if (typeof resources === 'object' || resources instanceof Resources) {
                this.Resources = resources;
            }
        }

        if (policies) {
            this.Policies = policies;
        }

        if (acls) {
            this.ACLs = acls;
        }
    }

    addPolicy(type, policy) {
        if (!type) {
            throw new Error('Missing type parameter');
        }

        if (!(typeof policy === "object")) {
            throw new Error('Missing policy parameter');
        }
        if (!this.hasOwnProperty("Policies")) {
            this.Policies = {};
        }

        this.Policies[type] = policy;
    }

    addOrg(org) {
        if (!org instanceof Organization) {
            throw new Error('org parameter not instance of Organization class');
        }

        if (!this.hasOwnProperty("Organizations")) {
            this.Organizations = [];
        }

        this.Organizations.push(org);
    }

    setResources(resources) {
        if (!resources instanceof Orderer) {
            throw new Error('resources parameter not instance of Resources class');
        }

        if (!this.hasOwnProperty("Resources")) {
            this.Resources = {};
        }

        if (typeof resources === 'string') {
            this.Resources = new Resources(resources);
        } else if (resources instanceof Resources) {
            this.Resources = resources;
        }
    }


};

module.exports = Application;