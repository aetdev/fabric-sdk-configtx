const Organization = require('./organization');

const Orderer = class {
    constructor(addresses, type, batchTimeout, batchSize, kafka, organizations, capabilities, maxChannels, policies) {

        if (!addresses) {
            throw new Error('Missing addresses parameter');
        }

        this.OrdererType = (type) ? type : "solo";
        this.BatchTimeout = (batchTimeout) ? batchTimeout : "2s";

        this.Organizations = null;

        if (organizations) {
            this.Organizations = [];

            if (Array.isArray(organizations)){
                this.Organizations = organizations;
            } else if (organizations instanceof Organization) {
                this.Organizations.push(organizations);
            } else {
                throw new Error('Missing organizations parameter');
            }


        }

        if (policies) {
            this.Policies = policies;
        }

        if (capabilities) {
            this.Capabilities = capabilities;
        }

        if (maxChannels) {
            this.MaxChannels = maxChannels;
        }

        if (addresses) {
            this.Addresses = [];

            if (typeof addresses === 'string') {
                this.Addresses.push(addresses);
            } else {
                this.Addresses = addresses;
            }
        }



        if (!batchSize) {
            this.BatchSize = {
                "MaxMessageCount": 10,
                "AbsoluteMaxBytes": "99 MB",
                "PreferredMaxBytes": "512 KB"
            }
        } else {
            this.BatchSize = batchSize;
        }

        if (!kafka) {
            this.Kafka = new Kafka("127.0.0.1:9092");
        } else if (kafka instanceof Kafka) {
            this.Kafka = kafka;
        } else if (typeof kafka === 'string' || Array.isArray(kafka)) {
            this.Kafka = new Kafka(kafka);
        }
    }

    addAddress(address) {
        if (typeof address !== 'string') {
            throw new Error('Missing address parameter');
        }

        this.Addresses.push(address);
    }


    addOrg(org) {
        if (!org instanceof Organization) {
            throw new Error('org parameter not instance of Organization class');
        }

        if (!this.hasOwnProperty("Organizations")) {
            this.Organizations = [];
        }

        this.Organizations.push(org);
    }

    addOrgs(orgsArr) {
        if (!orgsArr || !Array.isArray(orgsArr)) {
            throw new Error('Missing orgsArr parameter');
        }

        for (let i = 0; i < orgsArr.length; i++) {
            this.addOrg(orgsArr[i]);
        }
    }

    setKafka(kafka) {
        if (kafka instanceof Kafka) {
            this.Kafka = kafka;
        } else if (typeof kafka === 'string' || Array.isArray(kafka)) {
            this.Kafka = new Kafka(kafka);
        }
    }

    addPolicy(type, policy) {
        if (!type) {
            throw new Error('Missing type parameter');
        }

        if (!(typeof policy === "object")) {
            throw new Error('Missing policy parameter');
        }

        if (!this.hasOwnProperty("Policies")) {
            this.Policies = {};
        }

        this.Policies[type] = policy;
    }
};

const Kafka = class{
    constructor (brokers) {
        if (brokers) {
            this.Brokers = [];

            if (typeof brokers === 'string') {
                this.Brokers.push(brokers);
            } else if (Array.isArray(brokers)) {
                this.Brokers = brokers;
            }
        }
    }

    addBroker(broker) {
        if (typeof broker !== 'string') {
            throw new Error('Missing broker parameter');
        }

        if (!this.hasOwnProperty("Brokers")) {
            this.Brokers = [];
        }

        this.Brokers.push(broker);
    }
};



module.exports = Orderer;
module.exports.Kafka = Kafka;