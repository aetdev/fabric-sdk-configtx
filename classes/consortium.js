const Organization = require('./organization');

const Consortium = class {
    constructor(organizations) {
        if (!organizations) {
            throw new Error('Missing organizations parameter');
        }

        if (Array.isArray(organizations)) {
            this.Organizations = organizations;
        } else if (organizations instanceof Organization) {
            this.Organizations = [organizations];
        }
    }

    addOrg(org) {
        if (!org instanceof Organization) {
            throw new Error('org parameter not instance of Organization class');
        }

        if (!this.hasOwnProperty("Organizations")) {
            this.Organizations = [];
        }

        this.Organizations.push(org);
    }
};

module.exports = Consortium;